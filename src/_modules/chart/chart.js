
export default class Chart {
  constructor() {
    this.chart = document.querySelector('.chart');

    this.gr = this.chart.getContext("2d");
    this.maxCount = 35 + 10;
    this.x0 = 30;
    this.y0 = 20;
    this.width = this.chart.width - 80;
    this.height = this.chart.height - 90;
    this.stepY = Math.round(this.height / this.maxCount);
    this.stepX = Math.round(this.width / 10);

    this.m = 0;
    this.x_max = 10;

    this.grafics = {
      g1: {1: 10, 2: 4, 3: 15, 4: 22, 5: 16, 6: 20, 7: 18, 8: 13, 9: 16, 10: 35},
      g2: {1: 11, 2: 10, 3: 8, 4: 6, 5: 13, 6: 12, 7: 22, 8: 18, 9: 16, 10: 15},
      g3: {1: 5, 2: 4, 3: 2, 4: 1, 5: 7, 6: 6, 7: 16, 8: 12, 9: 10, 10: 9},
      g4: {1: 3, 2: 4, 3: 8, 4: 12, 5: 15, 6: 18, 7: 21, 8: 22, 9: 25, 10: 27}
    };
    this.colors = ['#f00', '#0f0', '#00f', '#0ff'];
  }

  _initChart() {
    this.gr.beginPath();
    //Вертикальная линия
    this.gr.moveTo(this.x0, this.y0);
    this.gr.lineTo(this.x0, this.height + this.y0);

    //горизонтальная линия
    this.gr.lineTo(this.width + this.x0, this.height + this.y0);

    //нижняя разметка и цифры
    for (var i = this.x0; this.m < this.x_max; i += this.stepX) {
      this.m++;
      this.gr.moveTo(i, this.height + this.y0);
      this.gr.lineTo(i, this.height + this.y0 + 15);
      this.gr.fillText(this.m, i + 3, this.height + this.y0 + 15);
    }
    this.gr.lineWidth = 2;
    this.gr.stroke();
    this.gr.closePath();
  }

  _drawGraphics() {
    var nr_color = 0;
    for (var g in this.grafics) {
      this.gr.beginPath();

      for (var m in this.grafics[g]) {
        var count = this.grafics[g][m];
        var x = this.x0 + ((m - 1) * this.stepX);
        var y = this.y0 + (this.height - count * this.stepY);

        if (1 == m)
          this.gr.moveTo(x, y);
        else
          this.gr.lineTo(x, y);

        this.gr.arc(x, y, 2, 0, 2 * Math.PI, false);
        this.gr.fillText(count, x - 5, y - 5);//текст над точками
        this.gr.fillText(count, this.x0 - 15, y);//текст у боковой линии
      }

      this.gr.strokeStyle = this.colors[nr_color]; //цвет линии
      nr_color++;
      this.gr.lineWidth = 1;//толщина линии
      this.gr.stroke();
    }
  }

  init() {

    if (this.chart) {

      this._initChart();
      this._drawGraphics();

    }
  }
}
