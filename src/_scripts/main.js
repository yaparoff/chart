'use strict';

import $ from 'jquery';
import Chart from '../_modules/chart/chart';


class Main {

  constructor() {
    this.chart = document.querySelector('.chart');

  }

  init() {

    if (this.chart) {
      const chart = new Chart();
      chart.init();
    }
  }
}

document.addEventListener('DOMContentLoaded', () => {
  const main = new Main();

  main.init();

});
